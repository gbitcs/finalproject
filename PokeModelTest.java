import static org.junit.Assert.*; 

import org.junit.Test;

public class PokeModelTest {

	@Test
	public void testGetPokemon1() {
		PokeModel test = new PokeModel();
		assertEquals(true, test.getPokemon("1"));
	}

	@Test
	public void testGetPokemon2() {
		PokeModel test = new PokeModel();
		assertEquals(false, test.getPokemon("999"));
	}

	@Test
	public void testGetType() {
		PokeModel test = new PokeModel();
		test.getPokemon("1");
		assertEquals("poison, grass", test.getType());
	}

	@Test
	public void testGetNumber() {
		PokeModel test = new PokeModel();
		test.getPokemon("1");
		assertEquals(1, test.getNumber());
	}

	@Test
	public void testGetBaseExp() {
		PokeModel test = new PokeModel();
		test.getPokemon("1");
		assertEquals(64, test.getBaseExp());
	}

}
