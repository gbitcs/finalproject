import java.net.URL; 
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class PokeController implements Initializable {

	@FXML
	private TextField text;
	
	@FXML
	private Button getPokemon;
	
	@FXML
	private Label name;
	
	@FXML
	private Label number;
	
	@FXML
	private Label type;
	
	@FXML
	private Label exp;
	
	@FXML
	private void buttonAction(ActionEvent e)
	{
		// Create object to access the model
		PokeModel pokemon = new PokeModel();
		
		// Has the button getPokemon been pressed?
		if (e.getSource() == getPokemon)
		{
			String id = text.getText();
			if (pokemon.getPokemon(id))
			{
				name.setText(pokemon.getName());
				number.setText(String.valueOf(pokemon.getNumber()));
				type.setText(pokemon.getType());
				exp.setText(String.valueOf(pokemon.getBaseExp()));
			}
			else
			{
				name.setText("Invalid ID");
				number.setText(null);
				type.setText(null);
				exp.setText(null);
			}
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
