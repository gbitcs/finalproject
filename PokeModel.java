import java.io.BufferedReader;   
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.*;

public class PokeModel {

	private JsonElement jse = null;
	
	public boolean getPokemon(String id)
	{
		try
		{
			URL url = new URL("http://pokeapi.salestock.net/api/v2/pokemon/" + id);

			// Open connection
			InputStream is = url.openStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			
			// Read results into JSON Element
			jse = new JsonParser().parse(br);
			
			// Close connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
	    {
	      uee.printStackTrace();
	    }
	    catch (java.net.MalformedURLException mue)
	    {
	      mue.printStackTrace();
	    }
	    catch (java.io.IOException ioe)
	    {
	      
	    }
	    catch (java.lang.NullPointerException npe)
	    {
	      npe.printStackTrace();
	    }
		
		return isValid();
	}
	
	public boolean isValid()
	  {
	    // If the id is valid we should find a name
	    try {
	      String error = jse.getAsJsonObject().get("name").getAsString();
	      return true;
	    }

	    catch (java.lang.NullPointerException npe)
	    {
	      // We could not find name meaning it is an invalid id
	      return false;
	    }
	  }
	
	public String getName()
	{
		String name = jse.getAsJsonObject().get("name").getAsString();
		return name;
	}
	
	public int getNumber()
	{
		int number = jse.getAsJsonObject().get("id").getAsInt();
		return number;
	}
	
	public String getType()
	{
		String type = jse.getAsJsonObject().get("types").getAsJsonArray().get(0).getAsJsonObject()
				.get("type").getAsJsonObject().get("name").getAsString();
		if (jse.getAsJsonObject().get("types").getAsJsonArray().size() > 1)
		{
			String type2 = jse.getAsJsonObject().get("types").getAsJsonArray().get(1).getAsJsonObject()
					.get("type").getAsJsonObject().get("name").getAsString();
			type = type + ", " + type2;
		}
		return type;
	}
	
	public int getBaseExp()
	{
		int exp = jse.getAsJsonObject().get("base_experience").getAsInt();
		return exp;
	}
}
