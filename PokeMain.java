import javafx.application.Application; 
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;


public class PokeMain extends Application {
	
	public void start(Stage primaryStage) throws Exception {
		try {
			Parent root = FXMLLoader.load(PokeMain.class.getResource("./PokeView.fxml"));
			Scene scene = new Scene(root);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("CSCI 13 Final Project - Muhammad Ali");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
